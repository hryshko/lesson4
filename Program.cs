﻿namespace Lesson4;

using System;
using System.Linq;
internal class Program
{
    static void Main(string[] args)
    {
        Task0();
        Task1();
        Task2();
        Task3();
    }
        static void Task0()
        {
            int[] numbers = { 1, 2, 3, 7 };
            Console.WriteLine("Please input any number.");
            int input = int.Parse(Console.ReadLine());

            bool found = false;

            foreach (int number in numbers)
            {
                if (number == input)
                { found = true; break; }
            }

            if (found)
            {
                Console.WriteLine("Your number is in the array!");
            }
            else
            {
                Console.WriteLine("Your number is no in the array!");
            }
        }

        static void Task1()
        {
            int[] numbers = { 1, 2, 3, 7, 563, 1094 };
            Console.WriteLine("Please input any number.");
            int input = int.Parse(Console.ReadLine());

            int[] filteredNumbers = numbers.Where(number => number != input).ToArray();

            if (filteredNumbers.Length == numbers.Length)
            {
                Console.WriteLine("Your number is not found in the array!");
            }
            else
            {
                Console.WriteLine("Your number is in the array! I deleted your number from the array and created a new array. Now the new array is look like this:");
                foreach (int number in filteredNumbers)
                { Console.WriteLine(number); }
            }
        }
        static void Task2()
        {
            Console.WriteLine("Please input any number. It will be a size of an array");
            int arraySize = int.Parse(Console.ReadLine());

            int[] array = new int[arraySize];
            Random random = new Random();
            for (int i = 0; i < arraySize; i++)
            {
                array[i] = random.Next(1, 1000);
            }

            int max = array[0];
            int min = array[0];
            int sum = 0;

            foreach (int number in array)
            {
                if (number > max)
                {
                    max = number;
                }
                if (number < min)
                {
                    min = number;
                }
                sum += number;
            }

            double average = (double)sum / (double)arraySize;

            Console.WriteLine("Maximum value is " + max);
            Console.WriteLine("Minimum value is " + min);
            Console.WriteLine("arithmetic average mean is " + average);
        }

        static void Task3()
        {
            int[] array1 = { 1, 18, 26, 88, 99 };
            int[] array2 = { 7, 27, 46, 77, 90 };

            Console.WriteLine("The first array is:");
            PrintArray(array1);

            Console.WriteLine("\nThe second array is:");
            PrintArray(array2);

            double average1 = CalculateAverage(array1);
            double average2 = CalculateAverage(array2);

            Console.WriteLine("\nArithmetic average mean of the first array: " + average1);
            Console.WriteLine("Arithmetic average mean of the second array: " + average2);

            if (average1 > average2)
            {
                Console.WriteLine("The arithmetic average mean of the first array is greater than the second array.");
            }
            else if (average2 > average1)
            {
                Console.WriteLine("The arithmetic average mean of the second array is greater than the first array.");
            }
            else
            {
                Console.WriteLine("The arithmetic averages of both arrays are equal.");
            }
        }

        static void PrintArray(int[] arr)
        {
            foreach (int num in arr)
            {
                Console.Write(num + " ");
            }
        }

        static double CalculateAverage(int[] arr)
        {
            int sum = 0;
            foreach (int num in arr)
            {
                sum += num;
            }
            return (double)sum / arr.Length;
        }
    } 